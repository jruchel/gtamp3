import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<String> targetDirs = new ArrayList<>();

        targetDirs.add("C:\\Users\\admin\\Documents\\Rockstar Games\\GTA V\\User Music");

        for (String s : args) {
            targetDirs.add(s);
        }

        String path = "C:\\Users\\admin\\Desktop\\GTA muzyka";


        MP3Mapper mp3Mapper = new MP3Mapper();
        List<String> temp = getFilesAsList(path);
        mp3Mapper.run(targetDirs, temp);
    }

    private static List<String> getFilesAsList(String path) {
        File f = new File(path);
        File[] files = f.listFiles();

        List<File> pathsList = Arrays.asList(files);

        pathsList = pathsList.stream().filter(file -> {
            String path1 = file.getAbsolutePath();
            return FilenameUtils.getExtension(path1).equals("mp3");
        }).collect(Collectors.toList());

        List<String> strings = new ArrayList<>();

        for (File file : pathsList) {
            strings.add(file.getAbsolutePath());
        }

        return strings;
    }

}
