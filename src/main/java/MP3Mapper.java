import com.mpatric.mp3agic.*;
import javafx.util.Pair;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class MP3Mapper implements Runnable {


    private String getTitle(String path) {
        String title = cutExtension(cutPath(path)).split("-")[1];
        return title;
    }

    private String getAuthor(String path) {
        String author = cutExtension(cutPath(path)).split("-")[0];

        return author;
    }


    private String cutExtension(String path) {
        return path.replace(".mp3", "");

    }

    private String cutPath(String path) {
        try {
            String[] temp = path.split(Pattern.quote(System.getProperty("file.separator")));
            return temp[temp.length - 1];
        } catch (NullPointerException ex) {
            return path;
        }

    }

    private void setTitleAndAuthor(String path, List<String> targetDirs) {
        try {
            String title = getTitle(path);
            String author = getAuthor(path);
            Pair pair = init(path);
            ID3v1 tag = (ID3v1) pair.getValue();
            Mp3File file = (Mp3File) pair.getKey();

            tag.setTitle(title);
            tag.setArtist(author);

            for (String s : targetDirs) {
                saveToDirectory(file, s, author, title);
            }
        } catch (Exception ex) {
            System.out.println(path);
        }
    }

    private void saveToDirectory(Mp3File file, String dir, String author, String title) throws IOException, NotSupportedException {
        file.save(dir + "\\" + author + "-" + title + ".mp3");
    }

    private Pair<Mp3File, ID3v1> init(String path) throws InvalidDataException, IOException, UnsupportedTagException {
        Mp3File file = new Mp3File(path);
        ID3v1 id3v1Tag;
        if (file.hasId3v1Tag()) {
            id3v1Tag = file.getId3v1Tag();
        } else {
            id3v1Tag = new ID3v1Tag();
            file.setId3v1Tag(id3v1Tag);
        }

        return new Pair<>(file, id3v1Tag);
    }


    @Override
    public void run(List<String> targetDirs, String... args) {
        run(targetDirs, Arrays.asList(args));
    }

    @Override
    public void run(List<String> targetDirs, List<String> args) {
        for (String s : args) {
            try {
                setTitleAndAuthor(s, targetDirs);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}


