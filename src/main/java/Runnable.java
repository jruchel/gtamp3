import java.util.List;

public interface Runnable {

    void run(List<String> targetDirs, String... args);

    void run(List<String> targetDirs, List<String> args);

}
